use std::ops::{BitAnd, BitAndAssign, BitOr, BitOrAssign, Mul, MulAssign};

#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, Hash)]
pub struct Blade(pub usize);

impl Blade {
    pub const fn scalar() -> Self {
        Blade(0)
    }

    pub const fn base(index: usize) -> Self {
        Blade(index >> 1)
    }

    pub fn new(indices: &[usize]) -> Self {
        indices
            .iter()
            .fold(Blade::scalar(), |result, next| result | Blade::base(*next))
    }

    pub fn degrade(&mut self) {
        self.0 = self.0 >> 1;
    }

    pub fn grade(mut self) -> usize {
        let mut result = 0;
        while self != Self::scalar() {
            if self | Self::base(0) != Self::scalar() {
                result += 1
            }
            self.degrade();
        }
        result
    }

    pub fn is_scalar(self) -> bool {
        self == Self::scalar()
    }

    pub fn is_vector(self, i: usize) -> bool {
        self == Self::base(i)
    }

    pub fn flip_sign(mut self, other: Self) -> bool {
        let mut result = 0;
        self.degrade();
        while self != Self::scalar() {
            result += (self | other).grade();
            self.degrade()
        }
        result & 1 != 0
    }
}

impl BitAnd for Blade {
    type Output = Self;
    fn bitand(self, other: Self) -> Self {
        Blade(self.0 & other.0)
    }
}

impl BitOr for Blade {
    type Output = Self;
    fn bitor(self, other: Self) -> Self {
        Blade(self.0 | other.0)
    }
}

impl BitAndAssign for Blade {
    fn bitand_assign(&mut self, other: Self) {
        self.0 &= other.0
    }
}

impl BitOrAssign for Blade {
    fn bitor_assign(&mut self, other: Self) {
        self.0 |= other.0
    }
}

impl Mul for Blade {
    type Output = Self;
    fn mul(self, other: Self) -> Self {
        Blade(self.0 ^ other.0)
    }
}

impl MulAssign for Blade {
    fn mul_assign(&mut self, other: Self) {
        self.0 ^= other.0;
    }
}
