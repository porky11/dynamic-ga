use crate::products::{self, BladeProduct};
use crate::Multivector;

use std::ops::{Add, Mul, Neg};

impl<T> Multivector<T>
where
    T: Add<Output = T> + Mul<Output = T> + Neg<Output = T> + Copy,
{
    pub fn multiply(self, other: Self, check: BladeProduct) -> Self {
        let mut result = Self::new();
        result.versor = self.versor && other.versor;
        for (blade1, value1) in self.elements {
            for (blade2, value2) in &other.elements {
                if check(blade1, *blade2) {
                    let mut value = value1 * *value2;
                    let blade = blade1 * *blade2;
                    if blade1.flip_sign(*blade2) {
                        value = -value;
                    }
                    if let Some(element) = result.elements.get_mut(&blade) {
                        *element = *element + value;
                    } else {
                        result.elements.insert(blade, value);
                    }
                }
            }
        }
        if !result.versor {
            result.versor = result.is_always_versor();
        }
        result
    }

    pub fn scalar(self, other: Self) -> Self {
        self.multiply(other, products::is_scalar)
    }

    pub fn inner(self, other: Self) -> Self {
        self.multiply(other, products::is_scalar)
    }

    pub fn dot(self, other: Self) -> Self {
        self.multiply(other, products::is_dot)
    }

    pub fn left_contraction(self, other: Self) -> Self {
        self.multiply(other, products::is_left_contraction)
    }

    pub fn right_contraction(self, other: Self) -> Self {
        self.multiply(other, products::is_right_contraction)
    }

    pub fn outer(self, other: Self) -> Self {
        self.multiply(other, products::is_outer)
    }

    pub fn geometric(self, other: Self) -> Self {
        self.multiply(other, products::is_geometric)
    }
}
