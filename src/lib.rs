use num_traits::{real::Real, Zero};
use std::collections::HashMap;
use std::ops::{Add, AddAssign, Div, DivAssign, Index, Mul, MulAssign, Neg, Sub, SubAssign};
use vector_space::{DotProduct, InnerSpace, VectorSpace};

pub mod blade;
pub mod multiplication;
pub mod products;

use blade::Blade;

#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Multivector<T> {
    elements: HashMap<Blade, T>,
    versor: bool,
}

impl<T> Multivector<T> {
    pub fn is_scalar(&self) -> bool {
        self.elements.len() == 1 && self.elements.contains_key(&Blade::scalar())
    }

    pub fn is_versor(&self) -> bool {
        self.versor
    }

    pub fn is_vector(&self) -> bool {
        for blade in self.elements.keys() {
            if blade.grade() != 1 {
                return false;
            }
        }
        true
    }

    pub fn new() -> Self {
        Multivector {
            elements: HashMap::new(),
            versor: true,
        }
    }

    pub fn from_scalar(value: T) -> Self {
        let mut elements = HashMap::new();
        elements.insert(Blade::scalar(), value);
        Multivector {
            elements,
            versor: true,
        }
    }

    pub fn from_base_vector(bases: Vec<T>) -> Self {
        let mut elements = HashMap::new();
        for (i, base) in bases.into_iter().enumerate() {
            elements.insert(Blade::base(i), base);
        }
        Multivector {
            elements,
            versor: true,
        }
    }

    fn is_always_versor(&self) -> bool {
        let count = self.elements.len();

        // simpe check for single blades
        if count == 1 {
            return true;
        }

        // simple check for vectors
        if self.is_vector() {
            return true;
        }

        // more advancedd check for complx versor types
        let (mut i, mut result, mut bitcount) = (0, 0, 0);
        loop {
            let mut finished = true;
            let (mut zero_count, mut zero_pos, mut one_count, mut one_pos) = (0, 0, 0, 0);

            for (n, blade) in self.elements.keys().enumerate() {
                if finished && !((1 << i) > blade.0) {
                    finished = false;
                }
                if blade.is_vector(i) {
                    one_pos = if one_count == 0 { n } else { one_pos };
                    one_count += 1;
                } else {
                    zero_pos = if zero_count == 0 { n } else { zero_pos };
                    zero_count += 1;
                }
            }
            if finished {
                break bitcount == count;
            }
            i += 1;
            if zero_count == 1 {
                let zero_bit = 1 << zero_pos;
                if zero_bit & result == 0 {
                    result |= zero_bit;
                    bitcount += 1;
                } else if one_count == 1 {
                    let one_bit = 1 << one_pos;
                    if one_bit & result == 0 {
                        result |= one_bit;
                        bitcount += 1
                    } else {
                        break bitcount == count;
                    }
                } else {
                    break bitcount == count;
                }
            } else if one_count == 1 {
                let one_bit = 1 << one_pos;
                if one_bit & result == 0 {
                    result |= one_bit;
                    bitcount += 1;
                } else {
                    break bitcount == count;
                }
            }
        }
    }
}

impl<T: Zero + Copy> Zero for Multivector<T> {
    fn zero() -> Self {
        Multivector {
            elements: HashMap::new(),
            versor: true,
        }
    }

    fn is_zero(&self) -> bool {
        for value in self.elements.values() {
            if !value.is_zero() {
                return false;
            }
        }
        true
    }
}

impl<T> Add<Self> for Multivector<T>
where
    T: Add<Output = T> + Copy,
{
    type Output = Self;
    fn add(mut self, mut other: Self) -> Self {
        let err = "When adding two multivectors, both vectors need to contain the same elements";
        for (blade, value) in self.elements.iter_mut() {
            *value = *value + other.elements.remove(blade).expect(err);
        }
        if !other.elements.is_empty() {
            panic!(err);
        }
        self.versor = self.is_always_versor();
        self
    }
}

impl<T> AddAssign<Self> for Multivector<T>
where
    T: AddAssign + Copy,
{
    fn add_assign(&mut self, mut other: Self) {
        let err = "When adding two multivectors, both vectors need to contain the same elements";
        for (blade, value) in self.elements.iter_mut() {
            *value += other.elements.remove(blade).expect(err);
        }
        if !other.elements.is_empty() {
            panic!(err);
        }
        self.versor = self.is_always_versor();
    }
}

impl<T> Sub<Self> for Multivector<T>
where
    T: Sub<Output = T> + Copy,
{
    type Output = Self;
    fn sub(mut self, mut other: Self) -> Self {
        let err =
            "When subtracting two multivectors, both vectors need to contain the same elements";
        for (blade, value) in self.elements.iter_mut() {
            *value = *value - other.elements.remove(blade).expect(err);
        }
        if !other.elements.is_empty() {
            panic!(err);
        }
        self.versor = self.is_always_versor();
        self
    }
}

impl<T> SubAssign<Self> for Multivector<T>
where
    T: SubAssign + Copy,
{
    fn sub_assign(&mut self, mut other: Self) {
        let err =
            "When subtracting two multivectors, both vectors need to contain the same elements";
        for (blade, value) in self.elements.iter_mut() {
            *value -= other.elements.remove(blade).expect(err);
        }
        if !other.elements.is_empty() {
            panic!(err);
        }
        self.versor = self.is_always_versor();
    }
}

impl<T> Neg for Multivector<T>
where
    T: Neg<Output = T> + Copy,
{
    type Output = Self;
    fn neg(mut self) -> Self {
        for value in self.elements.values_mut() {
            *value = -*value;
        }
        self
    }
}

impl<T> Mul<T> for Multivector<T>
where
    T: Mul<Output = T> + Copy,
{
    type Output = Self;
    fn mul(mut self, other: T) -> Self {
        for value in self.elements.values_mut() {
            *value = *value * other;
        }
        self
    }
}

impl<T> MulAssign<T> for Multivector<T>
where
    T: MulAssign + Copy,
{
    fn mul_assign(&mut self, other: T) {
        for value in self.elements.values_mut() {
            *value *= other;
        }
    }
}

impl<T> Div<T> for Multivector<T>
where
    T: Div<Output = T> + Copy,
{
    type Output = Self;
    fn div(mut self, other: T) -> Self {
        for value in self.elements.values_mut() {
            *value = *value / other;
        }
        self
    }
}

impl<T> DivAssign<T> for Multivector<T>
where
    T: DivAssign + Copy,
{
    fn div_assign(&mut self, other: T) {
        for value in self.elements.values_mut() {
            *value /= other;
        }
    }
}

impl<T> Mul for Multivector<T>
where
    T: Add<Output = T> + Mul<Output = T> + Neg<Output = T> + Copy,
{
    type Output = Self;
    fn mul(self, other: Self) -> Self {
        self.geometric(other)
    }
}

impl<T: Real> VectorSpace for Multivector<T> {
    type Scalar = T;
}

impl<T: Real> DotProduct for Multivector<T> {
    type Output = T;
    fn dot(self, other: Self) -> Self::Scalar {
        self.inner(other).elements[&Blade::scalar()]
    }
}

impl<T: Real> InnerSpace for Multivector<T> {}

impl<T> Index<&Blade> for Multivector<T> {
    type Output = T;
    fn index(&self, index: &Blade) -> &T {
        &self.elements[index]
    }
}
