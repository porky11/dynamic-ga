pub use crate::blade::Blade;

pub type BladeProduct = fn(a: Blade, b: Blade) -> bool;

pub const fn is_geometric(_a: Blade, _b: Blade) -> bool {
    true
}

fn is_contraction(a: Blade, b: Blade) -> bool {
    let a_grade = a.grade();
    let b_grade = b.grade();
    if a_grade > b_grade {
        return false;
    }
    (a * b).grade() == a_grade - b_grade
}

pub fn is_left_contraction(a: Blade, b: Blade) -> bool {
    is_contraction(a, b)
}

pub fn is_right_contraction(a: Blade, b: Blade) -> bool {
    is_contraction(a, b)
}

pub fn is_outer(a: Blade, b: Blade) -> bool {
    a & b == Blade::scalar()
}

pub fn is_scalar(a: Blade, b: Blade) -> bool {
    a * b == Blade::scalar()
}

pub fn is_dot(a: Blade, b: Blade) -> bool {
    is_contraction(a, b) || is_contraction(b, a)
}

pub fn is_inner(a: Blade, b: Blade) -> bool {
    is_dot(a, b) && !is_scalar(a, b)
}
